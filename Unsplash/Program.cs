﻿namespace Unsplash
{
    class Program
    {
        static void Main(string[] args)
        {
            var feedManager = new FeedManager();
            var url = feedManager.GetRandomWallpaperUrl();

            if (url != null && url.IsWellFormedOriginalString())
            {
                Wallpaper.Set(url, Wallpaper.Style.Center); //TODO make style available in config
            }
        }
    }
}
