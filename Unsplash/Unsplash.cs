﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;

namespace Unsplash
{
    //TODO add unit test
    public class FeedManager
    {
        private Uri RssFeed { get; set; }
        private readonly XDocument _feed;
        public FeedManager()
        {
            this.RssFeed = new Uri("https://unsplash.com/rss", UriKind.Absolute); //TODO add to app.config
            _feed = ReadFeed();
        }

        private static int GetRandomNumber(int start, int end)
        {
            var rnd = new Random();
            return rnd.Next(start, end);
        }

        private static XElement GetImageUrl(XElement element)
        {
            var image = element.Element("image");
            return image == null ? null : image.Element("url");
        }

        public Uri GetRandomWallpaperUrl()
        {
            if (_feed.Root == null) return null;

            var channel = _feed.Root.Element("channel");
            if (channel == null) return null;

            var items = channel.Elements("item").Select(GetImageUrl).ToList();
            if (!items.Any()) return null;

            var randomitem = items.ElementAt(GetRandomNumber(0, items.Count()));

            var uriBuilder = new UriBuilder(randomitem.Value);
            var query = HttpUtility.ParseQueryString("");
            query["w"] = "1920"; //TODO add to config
            uriBuilder.Query = query.ToString();

            return new Uri(uriBuilder.ToString());
        }

        private XDocument ReadFeed()
        {
            using (var webClient = new WebClient())
            {
                using (var stream = webClient.OpenRead(RssFeed))
                {
                    if (stream == null) return null;

                    using (var reader = new StreamReader(stream))
                    {
                        return XDocument.Load(reader);
                    }
                }
            }
        }
    }
}
